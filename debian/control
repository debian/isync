Source: isync
Section: mail
Priority: optional
Maintainer: Pierre-Elliott Bécue <peb@debian.org>
Standards-Version: 4.7.0
Build-Depends: debhelper-compat (= 13),
               libdb-dev,
               libsasl2-dev,
               libssl-dev,
               pkgconf,
               zlib1g-dev
Vcs-Git: https://salsa.debian.org/debian/isync.git
Vcs-Browser: https://salsa.debian.org/debian/isync
Homepage: https://isync.sourceforge.net/

Package: isync
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: mutt
Description: IMAP and MailDir mailbox synchronizer
 mbsync/isync is a command line application which synchronizes mailboxes;
 currently Maildir and IMAP4 mailboxes are supported. New messages, message
 deletions and flag changes can be propagated both ways. isync is suitable
 for use in IMAP-disconnected mode.
 .
 Features:
  * Fine-grained selection of synchronization operations to perform
  * Synchronizes single mailboxes or entire mailbox collections
  * Partial mirrors possible: keep only the latest messages locally
  * Trash functionality: backup messages before removing them
 IMAP features:
  * Security: supports TLS/SSL via imaps: (port 993) and STARTTLS; CRAM-MD5
    for authentication
  * Supports NAMESPACE for simplified configuration
  * Pipelining for maximum speed (currently only partially implemented)
